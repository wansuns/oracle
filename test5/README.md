# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID

```

## 实验步骤


### 创建一个包(Package)，包名是MyPack。
```
create or replace PACKAGE MYPACK2 AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);

END MYPACK2;
```
![](mypack.png)
### 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
```
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER 
  AS 
    -- TODO: FUNCTION MYPACK2.Get_SalaryAmount所需的实施
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
  END Get_SalaryAmount;

```
![](body.png)
### 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

```
 PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER) AS 
    -- TODO: PROCEDURE MYPACK2.Get_Employees所需的实施
      LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
  END Get_Employees;
```
![](body.png)
### 测试结果
#### 函数测试结果
![](fun.png)

#### 过程测试结果
![](pro.png)

## 实验总结

本次实验是sqldevelop和terminal终端结合使用的，因此，通过本次实验，我不仅了解了PL/SQL语言结构，PL/SQL变量和常量的声明和使用方法，包，过程，函数的用法。而且对于SQL develop的使用也更加熟练了，对于终端命令的运用也更加的娴熟。通过完成这个实验，我对PL/SQL语言结构，变量和常量的声明和使用方法，以及包、过程和函数的用法有了更深入的了解。

在实验过程中，我发现了自己的不足之处。首先，我需要更深入地学习SQL语言和PL/SQL语言的知识，在这基础上才能更加熟练地进行实验。其次，在编写代码时，我需要更加仔细地检查代码的每个部分，以确保代码的正确性。其次就是，数据库的大小写至今我仍然有点迷糊。但是，通过这次实验，我也收获了很多。我