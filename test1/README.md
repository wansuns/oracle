班级：软件工程-2   学号：202010414210  姓名：罗万阳
# 实验1：SQL语句的执行计划分析与优化指导
## 实验目的
分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用
## 实验数据库和用户
数据库是pdborcl，用户是sys和hr
## 实验内容
对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。
## 实验步骤
### 第一步，授权，先给hr授予权限
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr;

### 第二部，执行查询语句1
SQL> set autotrace on
SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
  6  GROUP BY d.department_name;
### 第三步，显示结果1
DEPARTMENT_NAME                部门总人数   平均工资
------------------------------ ---------- ----------
IT					5	5760
Sales				       34 8955.88235

执行计划
Plan hash value: 3808327043

--------------------------------------------------------------------------------
-------------------

| Id  | Operation		      | Name		  | Rows  | Bytes | Cost
 (%CPU)| Time	  |

--------------------------------------------------------------------------------
-------------------

|   0 | SELECT STATEMENT	      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 		  |	1 |    23 |
5  (20)| 00:00:01 |

|   2 |   NESTED LOOPS		      | 		  |    19 |   437 |
4   (0)| 00:00:01 |

|   3 |    NESTED LOOPS 	      | 		  |    20 |   437 |
4   (0)| 00:00:01 |

|*  4 |     TABLE ACCESS FULL	      | DEPARTMENTS	  |	2 |    32 |
3   (0)| 00:00:01 |

|*  5 |     INDEX RANGE SCAN	      | EMP_DEPARTMENT_IX |    10 |	  |
0   (0)| 00:00:01 |

|   6 |    TABLE ACCESS BY INDEX ROWID| EMPLOYEES	  |    10 |    70 |
1   (0)| 00:00:01 |

--------------------------------------------------------------------------------
-------------------

Predicate Information (identified by operation id):

   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note

    this is an adaptive plan

统计信息

	209  recursive calls
	 12  db block gets
	367  consistent gets
	  8  physical reads
       2044  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 15  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
### 第四步，执行查询语句2
SQL> set autotrace on
SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
  6  HAVING d.department_name in ('IT','Sales');
### 第五步，显示结果2
DEPARTMENT_NAME                部门总人数   平均工资
------------------------------ ---------- ----------
IT					                    5	5760
Sales				                34 8955.88235

执行计划

Plan hash value: 2128232041

--------------------------------------------------------------------------------
--------------

| Id  | Operation		       | Name	     | Rows  | Bytes | Cost (%CP
U)| Time     |

--------------------------------------------------------------------------------
--------------

|   0 | SELECT STATEMENT	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|*  1 |  FILTER 		       |	     |	     |	     |
  |	     |

|   2 |   HASH GROUP BY 	       |	     |	   1 |	  23 |	   7  (2
9)| 00:00:01 |

|   3 |    MERGE JOIN		       |	     |	 106 |	2438 |	   6  (1
7)| 00:00:01 |

|   4 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	  27 |	 432 |	   2   (
0)| 00:00:01 |

|   5 |      INDEX FULL SCAN	       | DEPT_ID_PK  |	  27 |	     |	   1   (
0)| 00:00:01 |

|*  6 |     SORT JOIN		       |	     |	 107 |	 749 |	   4  (2
5)| 00:00:01 |

|   7 |      TABLE ACCESS FULL	       | EMPLOYEES   |	 107 |	 749 |	   3   (
0)| 00:00:01 |

--------------------------------------------------------------------------------
--------------

Predicate Information (identified by operation id):


   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

统计信息

	187  recursive calls
	  0  db block gets
	281  consistent gets
	  6  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 14  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
### 第六步，优化指导
![优化指导](优化指导截图.png)
## 实验分析
对于这两个SQL语句，它们都是查询部门名称为"IT"或者"Sales"的部门的部门总人数和平均工资。它们的主要区别在于第一个SQL语句使用了where子句来筛选部门，而第二个SQL语句使用了having子句。
从执行计划的角度来看，第一个SQL语句使用了adaptive plan，而第二个SQL语句没有使用adaptive plan。在第一个SQL语句的执行计划中，可以看到使用了filter操作符来筛选部门名称为"IT"或者"Sales"的部门，而第二个SQL语句使用了having操作符来筛选部门。因此，第一个SQL语句的执行计划可能会比第二个SQL语句的执行计划更好，因为使用filter操作符来筛选数据通常比使用having操作符更有效率。
然而，从统计信息来看，第二个SQL语句的consistent gets次数比第一个SQL语句少，这意味着第二个SQL语句访问了更少的数据块。此外，第二个SQL语句使用了排序，因此在处理结果集时可能会消耗一些额外的资源。综合这些因素，我们可以认为第二个SQL语句的执行效率更高。
在sqldevelop中没有对第二个语句进行优化。
## 实验总结
通过本次实验，我学会了怎样去分析SQL执行计划，以及执行SQL语句的优化指导，理解了SQL语句的执行计划的重要作用。
知道了sql执行计划中各个参数的含义以及各个参数的指标，对于统计信息里的参数也有了一定的认知。
并且对于使用sqldevelop对于sql语句进行分析和优化指导，并且进行改进，对于他的运用更加熟悉。
分析这两者，大多从sql操作，cost，time进行，而统计信息则是从 db block gets， physical reads
consistent gets，这三个进行分析，评价sql语句的优劣。