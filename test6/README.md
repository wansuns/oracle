# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

## 实验目的
- 期末考核
## 实验内容
  - 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间使用方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。


## 实验步骤

### 1. 设计表及表空间方案，至少两个表空间，至少4张表，总的模拟数据量不少于10万条

在本次实验中首先先创建几个表空间，然后再根据需求分析创建表，最后再插入数据，具体情况如图所示：

##### 表空间的查询

![](./image/tablespace.png)

![](./image/tablespacename.png)

##### 根据需求分析创建表

![](./image/createtable1.png)

以下是部分代码：


```
 CREATE TABLE "SALE"."PRODUCTS" 
   (	"PRODUCT_ID" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	"PRODUCT_NAME" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	"PRODUCT_TYPE" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "PRODUCT_PK" PRIMARY KEY ("PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
产品


 


  CREATE TABLE "SALE"."EMPLOYEES" 
   (	"EMPLOYEE_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"NAME" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	"EMAIL" VARCHAR2(40 BYTE), 
	"PHONE_NUMBER" VARCHAR2(40 BYTE), 
	"HIRE_DATE" DATE NOT NULL ENABLE, 
	"SALARY" NUMBER(8,2), 
	"DEPARTMENT_ID" NUMBER(6,0), 
	 CONSTRAINT "EMPLOYEE_PK" PRIMARY KEY ("EMPLOYEE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE, 
	 CONSTRAINT "EMPLOYEE_DEPT_FK" FOREIGN KEY ("DEPARTMENT_ID")
	  REFERENCES "SALE"."DEPARTMENTS" ("DEPARTMENT_ID") ON DELETE SET NULL ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
员工


  CREATE TABLE "SALE"."DEPARTMENTS" 
   (	"DEPARTMENT_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"DEPARTMENT_NAME" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "DEPARTMENT_PK" PRIMARY KEY ("DEPARTMENT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
部门
```


##### 员工表

![](./image/employee_table.png)


##### 以下是部分建表阿插入数据截图
###### 部门表

![](./image/insert_dep.png)

![](./image/table_department.png)

###### 总表数以及员工数据插入

![](./image/table_toatal.png)
![](./image/insert_employees.png)

### 2. 设计权限及用户分配方案，至少两个用户

![](./image/tablespaceanduser.png)
这里可以看到已经有一些用户和角色了，所以我只创建了三个
![](./image/user.png)
![](./image/2user.png)
这里是角色的创建和授权
![](./image/create_role.png)

### 3. 建立程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑

现在则是创建程序包
![](./image/create_pack.png)
包的主体，包含一些存储过程和函数
![](./image/packbody.png)
大略有以下功能：
```
1. Get_DepartmentEmployeesCount: 统计特定部门的员工数量。
2. Get_HighestSalaryInDepartment: 用于获取特定部门中的最高工资，
3. Get_EmployeeDetails: 用于获取一个员工的详细信息，
4. Update_EmployeeSalary: 用于更新特定员工的薪资信息。
5. Insert_NewEmployee: 用于插入新员工记录。
6. Get_Employees(V_EMPLOYEE_ID NUMBER)通过查询员工表，递归查询某个员工及其所有下属
7. Get_AverageSalaryByDepartment：用于获取每个部门的平均工资。
8. Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，通过查询员工表，统计每个部门的salay工资总额。
```

以下是部分代码：

##### Get_DepartmentEmployeesCount:
```
CREATE OR REPLACE FUNCTION Get_DepartmentEmployeesCount(
  p_department_id IN departments.department_id%TYPE
)
RETURN NUMBER IS
  v_employees_count NUMBER(10);
BEGIN
  SELECT COUNT(*) INTO v_employees_count
  FROM employees
  WHERE department_id = p_department_id;

  RETURN v_employees_count;
END;

```
##### Get_HighestSalaryInDepartment:
```

CREATE OR REPLACE FUNCTION Get_HighestSalaryInDepartment(
  p_department_id IN departments.department_id%TYPE
)
RETURN NUMBER IS
  v_highest_salary NUMBER(10, 2);
BEGIN
  SELECT MAX(salary) INTO v_highest_salary
  FROM employees
  WHERE department_id = p_department_id;

  RETURN v_highest_salary;
END;

```
### 4. 设计一套数据库备份方案
设计一套数据库备份方案的基本步骤可以包括以下几个方面：
- 定义备份策略：在备份数据库之前，需要明确备份的目的和需要备份的数据。根据不同的需求，可以选择全备份、差异备份或增量备份等备份策略。
- 选择备份工具：根据备份策略的需求，选择合适的备份工具。Oracle数据库自带了备份工具RMAN（Recovery Manager），该工具提供了全备份、差异备份和增量备份等灵活的备份方式，同时还支持自动备份和恢复操作。
- 选择备份存储介质：备份存储介质需要选择可靠、高效的存储设备，例如硬盘、磁带、网络存储等。需要注意的是，备份数据存储的位置要有足够的空间，同时需要进行定期检查和维护。
- 制定恢复计划：备份数据的目的是为了在意外情况下可以顺利地进行数据恢复。因此，在备份之前，需要制定好有效的恢复计划，包括备份数据的恢复测试、灾难恢复和数据恢复过程的监控和记录等。
- 定期备份和测试：备份的目的是为了数据恢复，如果备份的数据不能成功恢复，备份就毫无意义。因此，在备份数据库之后，需要定期测试备份数据是否能成功恢复，以及测试恢复数据的速度和质量等。
- 
总体来说，设计一套数据库备份方案需要考虑备份策略、备份工具、备份存储介质、恢复计划以及定期备份和测试等方面，从而确保数据库备份和恢复的高可靠性和高效性。

因为这个销售系统的设计是在虚拟机上进行的，因此我们选择进行全备份，工具则是rman并进行测试
以下是部分备份的截图：
![](./image/backup.png)
![](./image/beifen.png)

## 实验结果

本次实验，设计了成功设计了基于Oracle数据库的商品销售系统的数据库设计方案，完成了任务，设计了5个表：部门表、产品表、员工表和订单表以及订单详表。其中，订单表包括订单编号、顾客姓名、顾客电话、订单日期、所属员工编号、订单整体优惠金额和订单应收货款，订单详表包括订单编号、产品编号、销售量和销售价格。设计了至少两个表空间，并成功创建了相应表。设计了至少两个用户，并为其分配了相应权限。在数据库中建立了一个程序包，并使用PL/SQL语言设计了一些存储过程和函数，使其能够处理比较复杂的业务逻辑。设计了一套数据库的备份方案。本次实验总模拟数据量不少于10万条，充分考验了我们的数据库设计和编写PL/SQL程序包的能力。通过本次实验，我们不仅掌握了Oracle数据库的各种操作和编程技术，还提高了我们的实际操作能力和实际项目经验




## 实验总结

过本次数据项目的设计，让我对Oracle数据库的应用有了更深入的了解和掌握。通过设计这样一个商品销售系统，我收获了很多，包括数据库设计能力的提升，在实验中，我需要设计数据表及表空间方案，充分考虑到数据规模和数据结构，满足实际业务需求，这需要对数据库设计的全面考虑和综合素质的提升。
PL/SQL语言的汲取。实验中需要用到PL/SQL语言编写存储过程及函数，这让我对PL/SQL语言特性、语法规则有了更深入的理解和应用。备份与恢复方案的设计。在实验中，我设计了一套数据库备份方案，熟悉了Oracle数据库备份和恢复的流程，掌握了关键信息的备份和恢复，提供了数据保障。
在实验中也遇到了一些问题，对于Oracle数据库的版本区别需要了解更加深入，避免一些设计上的错误；数据库设计方案考虑不够充分，添加了一些不必要的冗余数据，需要加强实际业务分析与设计思路； 存储过程和函数的设计还需要加强，考虑应用场景更全面化。
总体来说，通过本次实验，让我更深入地了解Oracle数据库的应用和实际操作，对数据库的设计、备份与恢复等方面都有了更多的收获和提高。



