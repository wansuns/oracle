# 实验三：创建分区表
姓名：罗万阳
学号“202010414210
## 实验目的
掌握分区表的创建方法，掌握各种分区方式的使用场景。
## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。
## 实验步骤
### 1. 由于实验二最后我们已经删除了实验角色和用户因此要重新创建并授权用户sale

```
SQL> CREATE ROLE con_res_role;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;

授权成功。
SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。
SQL> ALTER USER sale default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> GRANT con_res_role TO sale;

授权成功。
```
![](./tb_by_sql.png)
### 2. 使用sale用户创建两张表：订单表(orders)与订单详表(order_details)并且设置主键外键关联
```
CREATE TABLE orders (
    order_id NUMBER(9,0) NOT NULL,
    customer_name VARCHAR2(40 BYTE) NOT NULL,
    customer_tel VARCHAR2(40 BYTE) NOT NULL,
    order_date DATE NOT NULL,
    employee_id NUMBER(6,0) NOT NULL,
    discount NUMBER(8,2) DEFAULT 0,
    trade_receivable NUMBER(8,2) DEFAULT 0,
    CONSTRAINT ORDERS_PK PRIMARY KEY (ORDER_ID)
)
```
![](./PK.png)
```
CREATE TABLE order_details (
    id NUMBER(9,0) NOT NULL,
    order_id NUMBER(10,0) NOT NULL,
    product_id VARCHAR2(40 BYTE) NOT NULL,
    product_num NUMBER(8,2) NOT NULL,
    product_price NUMBER(8,2) NOT NULL,
    CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY (ID),
    CONSTRAINT ORDER_DETAILS_FK1 FOREIGN KEY (order_id)
        REFERENCES orders (order_id) ENABLE
)
TABLESPACE USERS PCTFREE 10 INITRANS 1
STORAGE (BUFFER_POOL DEFAULT)
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (ORDER_DETAILS_FK1);
```
![](./FK.png)
### 3. 创建B-Tree索引，它是默认的索引类型，如果不指定索引类型，创建的索引默认为B-Tree索引
![](./B_Tree.png)
### 4. 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值    
```
CREATE SEQUENCE  OERDER_OERDER_ID  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
CREATE SEQUENCE  OERDER_OERDERDETAILES_ID  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
![](./sequ.png)


### 5. orders表按订单日期（order_date）设置范围分区，order_details表设置引用分区
``` 
TABLESPACE USERS PCTFREE 10 INITRANS 1
STORAGE (BUFFER_POOL DEFAULT)
NOCOMPRESS NOPARALLEL
PARTITION BY RANGE (order_date) (
    PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
        TO_DATE('2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')
    ) NOLOGGING TABLESPACE USERS PCTFREE 10 INITRANS 1 STORAGE (
        INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS UNLIMITED BUFFER_POOL DEFAULT
    ) NOCOMPRESS NO INMEMORY,
    PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
        TO_DATE('2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')
    ) NOLOGGING TABLESPACE USERS,
    PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
        TO_DATE('2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')
    ) NOLOGGING TABLESPACE USERS
);
```
![](./fenqu.png)
### 6. 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）
```declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<410000 loop
    i := i+1;
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/
```
![](40.png)
```
DECLARE
  i INTEGER;
  y INTEGER := 2015;
  m INTEGER := 1;
  d INTEGER := 1;
  str VARCHAR2(100);
  order_id NUMBER(10, 0);
BEGIN
  i := 0;
  WHILE i < 2000000 LOOP 
    i := i + 1;
    m := m + 1;
    IF m > 12 THEN
      y := y + 1;
      m := 1;
    END IF;
    d := TRUNC(DBMS_RANDOM.VALUE(1, 28));
    str := y || '-' || LPAD(m, 2, '0') || '-' || Lpad(d, 2, '0');
    order_id := TRUNC(DBMS_RANDOM.VALUE(1, 410000));

    INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
    VALUES (SEQ2.nextval, order_id, 'p'||TRUNC(DBMS_RANDOM.VALUE(1, 40)), TRUNC(DBMS_RANDOM.VALUE(1, 10)), TRUNC(DBMS_RANDOM.VALUE(10, 100)));
    COMMIT;
  END LOOP;
END;
/
```
![](200.png)
### 7. 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划
INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id)
VALUES (1, 'Alice', '123456789', TO_DATE('2023-05-09', 'yyyy-mm-dd'), 1001);
COMMIT;
INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
VALUES (1, 1, 'p1', 2, 10);
COMMIT;

SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
WHERE o.order_id = 1;

EXPLAIN PLAN FOR 
SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
WHERE o.order_id = 1;

SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
## 实验总结
在这次实验中，我学习了如何创建分区表，并且掌握了不同分区方式适用的场景。通过比较使用有分区和无分区的SQL语句查询order数据量为40w，order_details数据量为200w时，发现分区表的优势更加明显。当数据量更大时，分区表的好处更加突出。但是，当数据量较小时，有分区和无分区的差异不太明显，有时无分区甚至会更快。